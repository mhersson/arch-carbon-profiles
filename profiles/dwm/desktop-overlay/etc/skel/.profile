# Input autorepeat
xset r rate 250 25
# Disable bell
xset b off

xsetroot -cursor_name left_ptr

# /opt/piavpn/bin/pia-client &
# ~/Scripts/setup_monitors
# feh --bg-scale ~/Pictures/wallpaper/kind-sur.jpg
dunst &
flashfocus &
picom --config ~/.config/picom/picom.conf -b
redshift &
# brave &
# flatpak run com.spotify.Client &
# dropbox &

# for i in {1..7}; do
#   alacritty &
# done
